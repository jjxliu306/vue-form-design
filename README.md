# vue-form-design

#### 介绍
本项目已经迁移到： https://gitee.com/jjxliu306/ng-form-element 

NG-FORM-ELEMENT

![组件一览](https://s3.ax1x.com/2020/12/22/rr6Dw6.png "11.png")
 
 **在线示例**
 http://jjxliu306.gitee.io/ng-form-element/

 **element-ui版本地址**
 https://gitee.com/jjxliu306/ng-form-element

 **iview版本地址**
 https://gitee.com/jjxliu306/ng-form-iview

  **element-plus版本地址**
 https://gitee.com/jjxliu306/ng-form-elementplus